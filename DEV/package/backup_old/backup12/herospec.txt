Bonus	Bonus	
(short)	(long)	Right Click Text
Archery	Secondary Skill Bonus: Archery	Receives a 5% per level bonus to Archery skill percentage.
Archers	Creature Bonus: Archers	Increases the Attack and Defense skills of any Archers or Marksmen for each level attained after 2nd level.
Griffins	Creature Bonus: Griffins	Increases the Attack and Defense skills of any Griffins or Royal Griffins or each level attained after 3rd level.
Learning	Secondary Skill Bonus: Learning	Receives a 5% per level bonus to Learning skill percentage.
Estates	Secondary Skill Bonus: Estates	Receives a 5% per level bonus to Estates skill.
Swordsmen	Creature Bonus: Swordsmen	Increases the Attack and Defense skills of any Swordsmen or Crusaders for each level attained after 4th level.
Ballista	Artillery Bonus: Ballista	Increases the Attack and Defense skill of any Ballista for each level attained after 4th level.
Cavaliers	Creature Bonus: Cavaliers	Increases the Attack and Defense skills of any Cavaliers or Champions for each level attained after 6th level.
First Aid	Secondary Skill Bonus: First Aid	Receives a 5% per level bonus to First Aid skill.
Bless	Spell Bonus: Bless	"Casts Bless with increased effect, based on hero level compared to the level of the target unit (the bonus is greater when used on weaker units)."
Astral Magic	Master of Astral Magic	Hero gets bonus to Astral Magic (25% + 5% per level).
Frost Ring	Spell Bonus: Frost Ring	"Casts Frost Ring with increased effect, based on hero level compared to the level of the target unit (the bonus is greater when used on weaker units)."
Monks	Creature Bonus: Monks	Increases the Attack and Defense skills of any Monks or Zealots for each level attained after 5th level.
Eagle Eye	Secondary Skill Bonus: Eagle Eye	Receives a 5% per level bonus to Eagle Eye skill percentage.
Prayer	Spell Bonus: Prayer	"Casts Prayer with increased effect, based on hero level compared to the level of the target unit (the bonus is greater when used on weaker units)."
Life Magic	Master of Life Magic	Hero gets bonus to Life Magic  (25% + 5% per level).
Armorer	Secondary Skill Bonus: Armorer	Receives a 5% per level bonus to Armorer skill percentage.
Ballista	Artillery Bonus: Ballista	Increases the Attack and Defense skill of any Ballista for each level attained after 4th level.
Learning	Secondary Skill Bonus: Learning	Receives a 5% per level bonus to Learning skill percentage.
Dendroids	Creature Bonus: Dendroids	Increases the Attack and Defense skills of any Dendroid Guards or Soldiers for each level attained after 5th level.
Resistance	Secondary Skill Bonus: Resistance	Receives a 5% per level bonus to Resistance skill percentage.
Elves	Creature Bonus: Elves	Increases the Attack and Defense skills of any Wood Elves or Grand Elves for each level attained after 3rd level.
Unicorns	Creature Bonus: Unicorns	Increases the Attack and Defense skills of any Unicorns or War Unicorns for each level attained after 6th level.
Logistics	Secondary Skill Bonus: Logistics	Receives a 5% per level bonus to Logistics skill percentage.
Slayer	Spell Bonus: Slayer	"Casts Slayer with increased effect, based on hero level compared to the level of the target unit (the bonus is greater when used on weaker units)."
Life Magic	Master of Life Magic	Hero gets bonus to Life Magic  (25% + 5% per level).
Intelligence	Secondary Skill Bonus: Intelligence	Receives a 5% per level bonus to Intelligence skill percentage.
First Aid	Secondary Skill Bonus: First Aid	Receives a 5% per level bonus to First Aid skill.
Eagle Eye	Secondary Skill Bonus: Eagle Eye	Receives a 5% per level bonus to Eagle Eye skill percentage.
Sorcery	Secondary Skill Bonus: Sorcery	Receives a 5% per level bonus to Sorcery skill percentage.
Ice Bolt	Spell Bonus: Ice Bolt	"Casts Ice Bolt with increased effect, based on hero level compared to the level of the target unit (the bonus is greater when used on weaker units)."
Pegasi	Creature Bonus: Pegasi	Increases the Attack and Defense skills of any Pegasi or Silver Pegasi for each level attained after 4th level.
Gargoyles	Creature Bonus: Gargoyles	Increases the Attack and Defense skills of any Stone or Obsidian Gargoyles for each level attained after 2nd level.
Genies	Creature Bonus: Genies	Increases the Attack and Defense skills of any Genies or Master Genies for each level attained after 5th level.
Golems	Creature Bonus: Golems	Increases the Attack and Defense skills of any Stone or Iron Golems for each level attained after 3rd level.
Armorer	Secondary Skill Bonus: Armorer	Receives a 5% per level bonus to Armorer skill percentage.
Ballista	Artillery Bonus: Ballista	Increases the Attack and Defense skill of any Ballista for each level attained after 4th level.
Nagas	Creature Bonus: Nagas	Increases the Attack and Defense skills of any Nagas or Naga Queens for each level attained after 6th level.
Learning	Secondary Skill Bonus: Learning	Receives a 5% per level bonus to Learning skill percentage.
Astral Magic	Master of Astral Magic	Hero gets bonus to Astral Magic (25% + 5% per level).
Hypnotize	Spell Bonus: Hypnotize	"Casts Hypnotize with increased effect, based on hero level compared to the level of the target unit (the bonus is greater when used on weaker units)."
Mysticism	Secondary Skill Bonus: Mysticism	Receives a 5% per level bonus to Mysticism skill.
Eagle Eye	Secondary Skill Bonus: Eagle Eye	Receives a 5% per level bonus to Eagle Eye skill percentage.
Clone	Spell Bonus: Clone	Spell Bonus: Clone
Magi	Creature Bonus: Magi	Increases the Attack and Defense skills of any Magi or Arch Magi for each level attained after 4th level.
Ch. Lightning	Spell Bonus: Chain Lightning	"Casts Chain Lightning with increased effect, based on hero level compared to the level of the target unit (the bonus is greater when used on weaker units)."
Haste	Spell Bonus: Haste	"Casts Haste with increased effect, based on hero level compared to the level of the target unit (the bonus is greater when used on weaker units)."
Sorcery	Secondary Skill Bonus: Sorcery	Receives a 5% per level bonus to Sorcery skill percentage.
Hell Hounds	Creature Bonus: Hell Hounds	Increases the Attack and Defense skills of any Hell Hounds or Cerberi for each level attained after 3rd level.
Efreet	Creature Bonus: Efreet	Increases the Attack and Defense skills of any Efreet or Efreet Sultans for each level attained after 6th level.
Demons	Creature Bonus: Demons	Increases the Attack and Defense skills of any Demons or Horned Demons for each level attained after 4th level.
Imps	Creature Bonus: Imps	Increases the Attack and Defense skills of any Imps or Familiars for each level attained after 1st level.
Learning	Secondary Skill Bonus: Learning	Receives a 5% per level bonus to Learning skill percentage.
Gogs	Creature Bonus: Gogs	Increases the Attack and Defense skills of any Gogs or Magogs for each level attained after 2nd level.
Ballista	Artillery Bonus: Ballista	Increases the Attack and Defense skill of any Ballista for each level attained after 4th level.
Pit Fiends	Creature Bonus: Pit Fiends	Increases the Attack and Defense skills of any Pit Fiends or Lords for each level attained after 5th level.
Intelligence	Secondary Skill Bonus: Intelligence	Receives a 5% per level bonus to Intelligence skill percentage.
Inferno	Spell Bonus: Inferno	"Casts Inferno with increased effect, based on hero level compared to the level of the target unit (the bonus is greater when used on weaker units)."
Mysticism	Secondary Skill Bonus: Mysticism	Receives a 5% per level bonus to Mysticism skill.
Astral Magic	Master of Astral Magic	Hero gets bonus to Astral Magic (25% + 5% per level).
Armageddon	Spell Bonus: Armageddon	"Casts Armageddon with increased effect, based on hero level compared to the level of the target unit (the bonus is greater when used on weaker units)."
Succubusses	Creature Bonus: Succubusses	Increases the Attack and Defense skills of any Succubusses for each level attained after 1st level.
Sorcery	Secondary Skill Bonus: Sorcery	Receives a 5% per level bonus to Sorcery skill percentage.
Fireball	Spell Bonus: Fireball	"Casts Fireball with increased effect, based on hero level compared to the level of the target unit (the bonus is greater when used on weaker units)."
Walking Dead	Creature Bonus: Walking Dead	Increases the Attack and Defense skills of any Walking Dead or Zombies for each level attained after 2nd level.
Vampires	Creature Bonus: Vampires	Increases the Attack and Defense skills of any Vampires or Vampire Lords for each level attained after 4th level.
Liches	Creature Bonus: Liches	Increases the Attack and Defense skills of any Liches or Power Liches for each level attained after 5th level.
Wights	Creature Bonus: Wights	Increases the Attack and Defense skills of any Wights or Wraiths for each level attained after 3rd level.
Black Knights	Creature Bonus: Black Knights	Increases the Attack and Defense skills of any Black Knights or Dread Knights for each level attained after 6th level.
Necromancy	Secondary Skill Bonus: Necromancy	Receives a 5% per level bonus to Necromancy skill percentage.
Soul Eaters	Creature Bonus: Soul Eaters	Increases the Attack and Defense skills of any Soul Eaters for each level attained after 1st level.
Skeletons	Creature Bonus: Skeletons	Increases the Attack and Defense skills of any Skeletons or Skeleton Warriors for each level attained after 1st level.
Death Ripple	Spell Bonus: Death Ripple	"Casts Death Ripple with increased effect, based on hero level compared to the level of the target unit (the bonus is greater when used on weaker units)."
Meteor Shower	Spell Bonus: Meteor Shower	"Casts Meteor Shower with increased effect, based on hero level compared to the level of the target unit (the bonus is greater when used on weaker units)."
Sorcery	Secondary Skill Bonus: Sorcery	Receives a 5% per level bonus to Sorcery skill percentage.
Eagle Eye	Secondary Skill Bonus: Eagle Eye	Receives a 5% per level bonus to Eagle Eye skill percentage.
Animate Dead	Spell Bonus: Animate Dead	"Casts Animate Dead with increased effect, based on hero level compared to the level of the target unit (the bonus is greater when used on weaker units)."
Stone Skin	Spell Bonus: Stone Skin	"Casts Stone Skin with increased effect, based on hero level compared to the level of the target unit (the bonus is greater when used on weaker units)."
Necromancy	Secondary Skill Bonus: Necromancy	Receives a 5% per level bonus to Necromancy skill percentage.
Town Portal	Spell Bonus: Town Portal	"Casts Town Portal with increased effect."
Harpies	Creature Bonus: Harpies	Increases the Attack and Defense skills of any Harpies or Harpy Hags for each level attained after 2nd level.
Ballista	Artillery Bonus: Ballista	Increases the Attack and Defense skill of any Ballista for each level attained after 4th level.
Minotaurs	Creature Bonus: Minotaurs	Increases the Attack and Defense skills of any Minotaurs or Minotaur Kings for each level attained after 5th level.
Beholders	Creature Bonus: Beholders	Increases the Attack and Defense skills of any Beholders or Evil Eyes for each level attained after 2nd level.
Offense	Secondary Skill Bonus: Offense	Receives a 5% per level bonus to Offense skill percentage.
Logistics	Secondary Skill Bonus: Logistics	Receives a 5% per level bonus to Logistics skill percentage.
Manticores	Creature Bonus: Manticores	Increases the Attack and Defense skills of any Manticores or Scorpicores for each level attained after 6th level.
Troglodytes	Creature Bonus: Troglodytes	Increases the Attack and Defense skills of any Troglodytes or Infernal Troglodytes for each level attained after 1st level.
Resurrection	Spell Bonus: Resurrection	"Casts Resurrection with increased effect, based on his level compared to the level of the target unit (the bonus is greater when used on weaker units)."
Mysticism	Secondary Skill Bonus: Mysticism	Receives a 5% per level bonus to Mysticism skill.
Sorcery	Secondary Skill Bonus: Sorcery	Receives a 5% per level bonus to Sorcery skill percentage.
Town Portal	Spell Bonus: Town Portal	"Casts Town Portal with increased effect."
Eagle Eye	Secondary Skill Bonus: Eagle Eye	Receives a 5% per level bonus to Eagle Eye skill percentage.
Meteor Shower	Spell Bonus: Meteor Shower	"Casts Meteor Shower with increased effect, based on hero level compared to the level of the target unit (the bonus is greater when used on weaker units)."
Learning	Secondary Skill Bonus: Learning	Receives a 5% per level bonus to Learning skill percentage.
Implosion	Spell Bonus: Implosion	"Casts Implosion with increased effect, based on hero level compared to the level of the target unit (the bonus is greater when used on weaker units)."
Cyclopes	Creature Bonus: Cyclopses	Increases the Attack and Defense skills of any Cyclopses or Cyclops Lords for each level attained after 6th level.
Ballista	Artillery Bonus: Ballista	Increases the Attack and Defense skill of any Ballista for each level attained after 4th level.
Orcs	Creature Bonus: Orcs	Increases the Attack and Defense skills of any Orcs or Orc Chieftains for each level attained after 3rd level.
Rocs	Creature Bonus: Rocs	Increases the Attack and Defense skills of any Rocs or Thunderbirds for each level attained after 5th level.
Goblins	Creature Bonus: Goblins	Increases the Attack and Defense skills of any Goblins or Hobgoblins for each level attained after 1st level.
Ogres	Creature Bonus: Ogres	Increases the Attack and Defense skills of any Ogres or Ogre Magi for each level attained after 4th level.
Offense	Secondary Skill Bonus: Offense	Receives a 5% per level bonus to Offense skill percentage.
Wolf Riders	Creature Bonus: Wolf Riders	Increases the Attack and Defense skills of any Wolf Riders or Raiders for each level attained after 2nd level.
Sorcery	Secondary Skill Bonus: Sorcery	Receives a 5% per level bonus to Sorcery skill percentage.
Ogres	Creature Bonus: Ogres	Increases the Attack and Defense skills of any Ogres or Ogre Magi for each level attained after 4th level.
Logistics	Secondary Skill Bonus: Logistics	Receives a 5% per level bonus to Logistics skill percentage.
Haste	Spell Bonus: Haste	"Casts Haste with increased effect, based on hero level compared to the level of the target unit (the bonus is greater when used on weaker units)."
Precision	Spell Bonus: Precision	"Casts Precision with increased effect, based on hero level compared to the level of the target unit (the bonus is greater when used on weaker units)."
Intelligence	Secondary Skill Bonus: Intelligence	Receives a 5% per level bonus to Intelligence skill percentage.
Eagle Eye	Secondary Skill Bonus: Eagle Eye	Receives a 5% per level bonus to Eagle Eye skill percentage.
Learning	Secondary Skill Bonus: Learning	Receives a 5% per level bonus to Learning skill percentage.
Basilisks	Creature Bonus: Basilisks	Increases the Attack and Defense skills of any Basilisks or Greater Basilisks for each level attained after 4th level.
Gnolls	Creature Bonus: Gnolls	Increases the Attack and Defense skills of any Gnolls or Gnoll Marauders for each level attained after 1st level.
Lizardmen	Creature Bonus: Lizardmen	Increases the Attack and Defense skills of any Lizardmen or Lizard Warriors for each level attained after 2nd level.
Armorer	Secondary Skill Bonus: Armorer	Tazar receives a 5% per level bonus to his Armorer skill percentage.
Gorgons	Creature Bonus: Gorgons	Increases the Attack and Defense skills of any Gorgons or Mighty Gorgons for each level attained after 5th level.
Serpent Flies	Creature Bonus: Serpent Flies	Increases the Attack and Defense skills of any Serpent or Dragon Flies for each level attained after 3rd level.
Ballista	Artillery Bonus: Ballista	Increases the Attack and Defense skill of any Ballista for each level attained after 4th level.
Wyverns	Creature Bonus: Wyverns	Increases the Attack and Defense skills of any Wyverns or Wyvern Monarchs for each level attained after 6th level.
Astral Magic	Master of Astral Magic	Hero gets bonus to Astral Magic (25% + 5% per level).
Mysticism	Secondary Skill Bonus: Mysticism	Receives a 5% per level bonus to Mysticism skill.
Learning	Secondary Skill Bonus: Learning	Receives a 5% per level bonus to Learning skill percentage.
First Aid	Secondary Skill Bonus: First Aid	Receives a 5% per level bonus to First Aid skill.
Stone Skin	Spell Bonus: Stone Skin	"Casts Stone Skin with increased effect, based on her level compared to the level of the target unit (the bonus is greater when used on weaker units)."
Sorcery	Secondary Skill Bonus: Sorcery	Receives a 5% per level bonus to Sorcery skill percentage.
Intelligence	Secondary Skill Bonus: Intelligence	Receives a 5% per level bonus to Intelligence skill percentage.
Eagle Eye	Secondary Skill Bonus: Eagle Eye	Receives a 5% per level bonus to Eagle Eye skill percentage.
Sorcery	Secondary Skill Bonus: Sorcery	Receives a 5% per level bonus to Sorcery skill percentage.
Pathfinding	Secondary Skill Bonus: Pathfinding	Receives a 5% per level bonus to Pathfinding skill percentage.
Also it restores Additional 600 Movement Points after Battle.
Gives tiny amount of experience every hero step.
Astral Magic	Master of Astral Magic	Hero gets bonus to Astral Magic (25% + 5% per level).
Water Ele.	Creature Bonus: Water Elementals	Water/Ice elementals receive +2 Att.
Psychic Ele.	Creature Bonus: Psychic Elementals	Psychic/Magic elementals receive +3 Att and +3 Def.
Earth Ele.	Creature Bonus: Earth Elementals	"Earth/Magma elementals receive +2 Att, +1 Def, and +5 Dmg."
Fire Ele.	Creature Bonus: Fire Elementals	"Fire/Energy elementals receive +1 Att, +2 Def, and +2 Dmg."
Learning	Secondary Skill Bonus: Learning	Receives a 5% per level bonus to Learning skill percentage.
Fire Wall	Spell Bonus: Fire Wall	Damage from Fire Walls is doubled.
Ch. Lightning	Spell Bonus: Chain Lightning	"Casts Chain Lightning with increased effect, based on hero level compared to the level of the target unit (the bonus is greater when used on weaker units)."
Magic Arrow	Spell Bonus: Magic Arrow	"When cast, Magic Arrow damage is increased 50%."
Town Portal	Spell Bonus: Town Portal	"Casts Town Portal with increased effect."
Summon Fire Elemental	Spell Bonus: Summon Fire Elemental	Spell Bonus: Summon Fire Elemental
Summon Air Elemental	Spell Bonus: Summon Air Elemental	Spell Bonus: Summon Air Elemental
Summon Water Elemental	Spell Bonus: Summon Water Elemental	Spell Bonus: Summon Water Elemental
Summon Earth Elemental	Spell Bonus: Summon Earth Elemental	Spell Bonus: Summon Earth Elemental
Speed	Creature Bonus: Speed	All creatures receive +2 speed.
Fire Magic	Secondary Skill Bonus: Exp. Fire Magic	Starts with Expert Fire Magic.
Nightmares	Creature Bonus: Nightmares	Increases the Attack and Defense skills of any Nightmare for each level attained after 4th level.
Enchanters	Creature Bonus: Enchanters	Can upgrade Monks or Zealots and Magi or Arch Magi to Enchanters.
Sharpshooters	Creature Bonus: Sharpshooters	Can upgrade Archers or Marksmen and Wood Elves or Grand Elves to Sharpshooters.
Behemoths	Creature Bonus: Behemoths	"Behemoths or Ancient Behemoths receive +5 Att, +5 Def, +10 Dmg."
Dracoliches
	Creature Bonus: Dracoliches	"Dracoliches receive +5 Att, +5 Def, and +10 Dmg."
Rust Dragons	Creature Bonus: Rust Dragons	Creature Bonus: Rust Dragons
Swordsmen	Creature Bonus: Swordsmen	Increases the Attack and Defense skills of any Swordsmen or Crusaders for each level attained after 4th level.
Dragons	Creature Bonus: Dragons	"All dragons receive +5 Att, +5 Def."
Werewolves	Creature Bonus: Werewolves	Increases the Attack and Defense skills of any Werewolf for each level attained after 4th level.
Devils	Creature Bonus: Devils	"Devils and Arch Devils receive +4 Attack, +2 Defense, and +1 Speed."
		 
