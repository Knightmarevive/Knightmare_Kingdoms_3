#include <allegro.h>
#include <cstdio>

extern FONT *font;

unsigned int fpsCounter=0;
void fpsCounterFunc(){
	fpsCounter++;
}
int main() {
/*//////////////////////////////////////////////////////////////*/
	int depth, res;
	allegro_init();
	set_color_depth(desktop_color_depth());
	res = set_gfx_mode(GFX_AUTODETECT_WINDOWED, 640, 480, 0, 0);
	if (res != 0) {
		allegro_message(allegro_error);
		exit(-1);
	}
	install_timer();
	install_keyboard();
	install_mouse();
	
	LOCK_VARIABLE(fpsCounter);
	LOCK_FUNCTION(fpsCounterFunc);
	install_int_ex(fpsCounterFunc,BPS_TO_TIMER(60));
	BITMAP* buffer=create_bitmap(640,480);
/*//////////////////////////////////////////////////////////////*/

	BITMAP* Ballista     = load_bitmap("HrArt124.bmp",nullptr);
	BITMAP* AmmoCart     = load_bitmap("HrArt125.bmp",nullptr);
	BITMAP* FirstAidTent = load_bitmap("HrArt126.bmp",nullptr);
	
	BITMAP* Ballista_plus     = create_bitmap(44,44);
	BITMAP* AmmoCart_plus     = create_bitmap(44,44);
	BITMAP* FirstAidTent_plus = create_bitmap(44,44);
	int level=1;

	while (!key[KEY_ESC]) {
		while (fpsCounter>0) {
/////////	///////////INPUT///////////////////////////////////


////////	//////////LOGIC////////////////////////////////////


/////////	///////////RENDERING///////////////////////////////
			draw_sprite(buffer,Ballista,10,10);
			draw_sprite(buffer,AmmoCart,10,110);
			draw_sprite(buffer,FirstAidTent,10,210);
			
			draw_sprite(Ballista_plus,Ballista,0,0);
			draw_sprite(AmmoCart_plus,AmmoCart,0,0);
			draw_sprite(FirstAidTent_plus,FirstAidTent,0,0);
			
			textprintf_ex(Ballista_plus,font,0,0,makecol(0,255,0),-1,"%02i",level);
			textprintf_ex(AmmoCart_plus,font,0,0,makecol(0,255,0),-1,"%02i",level);
			textprintf_ex(FirstAidTent_plus,font,0,0,makecol(0,255,0),-1,"%02i",level);
			
			if(level>0 && level < 75){
				char filename[4096];
				sprintf(filename,"HrArt%03i.bmp",1000 - 75 * 1 + level); //Ballista
				save_bitmap(filename,Ballista_plus,nullptr);
				
				sprintf(filename,"HrArt%03i.bmp",1000 - 75 * 2 + level); //Ammo Cart
				save_bitmap(filename,AmmoCart_plus,nullptr);
				
				sprintf(filename,"HrArt%03i.bmp",1000 - 75 * 3 + level); //First Aid Tent
				save_bitmap(filename,FirstAidTent_plus,nullptr);
				
				level++;
			}
			
			draw_sprite(buffer,Ballista_plus,110,10);
			draw_sprite(buffer,AmmoCart_plus,110,110);
			draw_sprite(buffer,FirstAidTent_plus,110,210);
			
			
			draw_sprite(screen,buffer,0,0);
			clear_bitmap(buffer);
////////	///////////FRAMES////////////////////////////
			fpsCounter=0;
			rest(1);
		}
	}
	return 0;
}END_OF_MAIN()
